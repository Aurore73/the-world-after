<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    public function test_comparestring()
    {
        $this->assertEquals("first unit test","first "."unit "."test");
        $this->assertEquals("second unit test", "second " . "unit " . "test");
    }

    public function test_comparenumber()
    {
        $this->assertEquals(2, 4/2);
    }
}