<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $faker;
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->faker = Factory::create("fr_FR");
        $this->encoder = $encoder;
    }

    private function uploadimagefromurl(User $user, string $url)
    {
        $info = pathinfo($url);
        $contents = file_get_contents($url);
        $filename = $info["basename"];
        $user->setDocument($filename);
        $dirnamedirectory = dirname(\dirname(__DIR__)) . "/public/images/";

        if (file_exists($dirnamedirectory)) {
            $directory = $dirnamedirectory . "users/";
        } else {
            mkdir($dirnamedirectory);
            $directory = $dirnamedirectory . "users/";
        }

        if (file_exists($directory)) {
            $file = $directory . $filename;
        } else {
            mkdir($directory);
            $file = $directory . $filename;
        }

        file_put_contents($file, $contents);
        $imagefile = new UploadedFile($file, $filename);
    }

    public function load(ObjectManager $manager)
    {
        $adminuser = new User();
        $adminuser->setEmail("administrator@gmail.com");
        $adminuser->setRoles(["ROLE_ADMIN"]);
        $adminuser->setPassword($this->encoder->encodePassword($adminuser, "administrator"));
        $adminuser->setFirstName($this->faker->firstName);
        $adminuser->setLastName($this->faker->lastName);
        $adminuser->setPersonalCode($this->faker->ean13);
        $adminuser->setPhone($this->faker->phoneNumber);
        $adminuser->setAddress($this->faker->address);
        $adminuser->setCity($this->faker->city);
        $slugify = new Slugify();
        $filename = $slugify->slugify($this->faker->firstName);
        $urlimg = "https://api.adorable.io/avatars/285/".$filename.".png";
        $this->uploadimagefromurl($adminuser,$urlimg);
        $manager->persist($adminuser);
        $manager->flush();

    }
}