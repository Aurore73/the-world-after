<?php

namespace App\Controller\Admin;

use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin",name="admin_")
 */
class AdminHomeController extends AbstractController
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @Route("/",name="home")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $admin = $this->user->findOneBy(['email' => $request->getSession()->get(Security::LAST_USERNAME)]);
        //dump($admin);
        return $this->render("admin/index.html.twig",compact("admin"));
    }

    /**
     * @Route("/profil/{id}",name="profil")
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showprofil(int $id)
    {
        $adminprofil = $this->user->findOneBy(['id' => $id]);
        //dump($adminprofil);
        return $this->render("admin/profil.html.twig", compact("adminprofil")); 
    }

    /**
     * @Route("/users",name="users")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userslist()
    {
        $users = $this->user->findAll();
        //dump($users);
        return $this->render("admin/user/users.html.twig", compact("users")); 
    }

    /**
     * @Route("/users/update/{id}", name="user_update")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function useredit(int $id, Request $request)
    {
        $user = $this->user->findOneBy(['id' => $id]);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $userForm = $form->createView();

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData()->getRoles());
            $entityManager = $this->getDoctrine()->getManager();
            $this->addFlash('success', 'Utilisateur modifié avec succès');
            $entityManager->flush();
            return $this->redirectToRoute('admin_users');
        }

        return $this->render("admin/user/useredit.html.twig",compact("userForm"));
    }
}
