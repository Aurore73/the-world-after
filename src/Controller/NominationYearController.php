<?php

namespace App\Controller;

use App\Entity\NominationYear;
use App\Form\NominationYearType;
use App\Repository\NominationYearRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nomination/year")
 */
class NominationYearController extends AbstractController
{
    /**
     * @Route("/", name="nomination_year_index", methods={"GET"})
     */
    public function index(NominationYearRepository $nominationYearRepository): Response
    {
        //dd($nominationYearRepository->findAll());
        return $this->render('nomination_year/index.html.twig', [
            'nomination_years' => $nominationYearRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="nomination_year_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $nominationYear = new NominationYear();
        $form = $this->createForm(NominationYearType::class, $nominationYear);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($nominationYear);
            $entityManager->flush();

            return $this->redirectToRoute('nomination_year_index');
        }

        return $this->render('nomination_year/new.html.twig', [
            'nomination_year' => $nominationYear,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nomination_year_show", methods={"GET"})
     */
    public function show(NominationYear $nominationYear): Response
    {
        return $this->render('nomination_year/show.html.twig', [
            'nomination_year' => $nominationYear,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="nomination_year_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NominationYear $nominationYear): Response
    {
        $form = $this->createForm(NominationYearType::class, $nominationYear);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nomination_year_index');
        }

        return $this->render('nomination_year/edit.html.twig', [
            'nomination_year' => $nominationYear,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nomination_year_delete", methods={"DELETE"})
     */
    public function delete(Request $request, NominationYear $nominationYear): Response
    {
        if ($this->isCsrfTokenValid('delete'.$nominationYear->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($nominationYear);
            $entityManager->flush();
        }

        return $this->redirectToRoute('nomination_year_index');
    }
}
