<?php

namespace App\Entity;

use App\Entity\Category;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * NominationYear
 *
 * @ORM\Table(name="nomination_year", indexes={@ORM\Index(name="IDX_6F1AD43912469DE2", columns={"category_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\NominationYearRepository")
 */
class NominationYear
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(name="date_nomination_year", type="datetime", nullable=false)
     */
    private $dateNominationYear;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="nominationYear")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Company", inversedBy="nominationYear")
     * @ORM\JoinTable(name="nomination_year_company",
     *   joinColumns={
     *     @ORM\JoinColumn(name="nomination_year_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     *   }
     * )
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity=Votes::class, mappedBy="company")
     */
    private $votes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->company = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->dateNominationYear = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateNominationYear(): ?\DateTimeInterface
    {
        return $this->dateNominationYear;
    }

    public function setDateNominationYear(\DateTimeInterface $dateNominationYear): self
    {
        $this->dateNominationYear = $dateNominationYear;

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompany(): Collection
    {
        return $this->company;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->company->contains($company)) {
            $this->company[] = $company;
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->company->contains($company)) {
            $this->company->removeElement($company);
        }

        return $this;
    }


    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Votes[]
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    public function addVote(Votes $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes[] = $vote;
            $vote->setNominationYear($this);
        }

        return $this;
    }

    public function removeVote(Votes $vote): self
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
            // set the owning side to null (unless already changed)
            if ($vote->getNominationYear() === $this) {
                $vote->setNominationYear(null);
            }
        }

        return $this;
    }
}
